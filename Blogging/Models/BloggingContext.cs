﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Blogging.Models
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=BloggingDB;Trusted_Connection=True;")
                .EnableSensitiveDataLogging(true)
                .UseLoggerFactory(new LoggerFactory()
                .AddConsole(LogLevel.Information));
        }
    }
}
