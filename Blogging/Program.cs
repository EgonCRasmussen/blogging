﻿using Blogging.DAL;
using System;

namespace Blogging
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var item in BlogRepository.GetBlogs())
            {
                Console.WriteLine($"BlogId: {item.BlogId} - Url: {item.Url}");
            }

            Console.WriteLine(BlogRepository.GetBlog(1).Url);

            //int id = BlogRepository.AddBlog("http://sample.com");
            //Console.WriteLine($"Ny blog oprettet med id: {id}");


            //if (BlogRepository.UpdateBlog(2))
            //{
            //    Console.WriteLine("Blog med id = 2 er updated");
            //}
            //else
            //{
            //    Console.WriteLine("Ingen blogs blev updated");
            //}

            //if (BlogRepository.DeleteBlog(2))
            //{
            //    Console.WriteLine("Blog med id = 2 er deleted");
            //}
            //else
            //{
            //    Console.WriteLine("Ingen blogs blev deleted");
            //}
        }
    }
}
