﻿using Blogging.Models;
using System.Collections.Generic;
using System.Linq;

namespace Blogging.DAL
{
    public class BlogRepository
    {
        public static List<Blog> GetBlogs()
        {
            using (var context = new BloggingContext())
            {
                return context.Blogs.ToList();
            }
        }

        public static Blog GetBlog(int id)
        {
            using (var context = new BloggingContext())
            {
                return context.Blogs.Single(b => b.BlogId == id);
            }
        }

        public static int AddBlog(string url)
        {
            using (var context = new BloggingContext())
            {
                var blog = new Blog { Url = url };
                context.Blogs.Add(blog);
                context.SaveChanges();
                return blog.BlogId;
            }
        }

        public static bool UpdateBlog(int id)
        {
            using (var context = new BloggingContext())
            {
                var blog = context.Blogs.Find(id);
                if (blog != null)
                {
                    blog.Url = "http://sampleupdated.com/blog";
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static bool DeleteBlog(int id)
        {
            using (var context = new BloggingContext())
            {
                var blog = context.Blogs.Find(id);
                if (blog != null)
                {
                    context.Blogs.Remove(blog);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}
